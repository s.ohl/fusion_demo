import cv2
import numpy as np
import math
from pprint import pprint, pformat

import rclpy
from sensor_msgs.msg import PointCloud2, Imu, Image
from geometry_msgs.msg import PointStamped
from rclpy.qos import qos_profile_sensor_data
from rclpy.node import Node
from rclpy.parameter import Parameter
from cv_bridge import CvBridge


def getAnglesFromQuadrion(o):
    # https://github.com/cyberbotics/webots/blob/1b8fdd6243a34c8a3d08a7c1bd84528e9e321609/src/controller/c/inertial_unit.c
    # ENU: extrensic rotation matrix e = Z(yaw) Y(pitch) X(roll)
    t0 = 2.0 * (o.w * o.x + o.y * o.z)
    t1 = 1.0 - 2.0 * (o.x * o.x + o.y * o.y)
    roll = math.atan2(t0, t1)
    t2 = 2.0 * (o.w * o.y - o.z * o.x)
    t2 = 1.0 if t2 > 1.0 else t2
    t2 = -1.0 if t2 < -1.0 else t2
    pitch = math.asin(t2)
    t3 = 2.0 * (o.w * o.z + o.x * o.y)
    t4 = 1.0 - 2.0 * (o.y * o.y + o.z * o.z)
    yaw = math.atan2(t3, t4)
    return roll, yaw, pitch


class LIDARGrid(Node):

    def __init__(self):
        super().__init__('lidar_grid')

        self.declare_parameter("vehicle", "")
        self.vehicle_topic = self.get_parameter(
            'vehicle').get_parameter_value().string_value

        self.set_parameters(
            [Parameter('use_sim_time', Parameter.Type.BOOL, True)])

        # ROS interface

        self.create_subscription(PointCloud2, 'myvehicle/lidar/point_cloud',
                                 self.__on_lidar, qos_profile_sensor_data)
        self.create_subscription(PointStamped, 'myvehicle/gps', self.__on_gps,
                                 qos_profile_sensor_data)
        self.create_subscription(Imu, 'imu', self.__on_imu,
                                 qos_profile_sensor_data)
        self._grid = self.create_publisher(Image, "myvehicle/grid", 1)

        self.timer = self.create_timer(0.1,
                                       self.__timer_callback,
                                       clock=self.get_clock())

        self.grid = np.zeros([100, 100])  # initialize as undecided
        self.resolution = [1, 1]  # resolution
        self.degradeValue = 0.05  # how fast will information be lost

    def __on_gps(self, message):
        self.get_logger().info("got gps %f,%f" %
                               (message.point.x, message.point.y))

    def __on_imu(self, message):
        roll, yaw, pitch = getAnglesFromQuadrion(message.orientation)
        self.get_logger().info("got imu r %f y %f p %f" % (roll, yaw, pitch))

    def __timer_callback(self):
        self.get_logger().info("timer")
        self.degrade()
        self._grid.publish(self.show())

    def __on_lidar(self, message):
        # magic conversion to numpy array
        np_dtype_list = []
        np_dtype_list.append(("x", np.dtype('float32')))
        np_dtype_list.append(("y", np.dtype('float32')))
        np_dtype_list.append(("z", np.dtype('float32')))
        np_dtype_list.append(("layer", np.dtype('int32')))
        np_dtype_list.append(("time", np.dtype('float32')))
        data = np.fromstring(message.data.tobytes(), np_dtype_list)

        for p in data:
            if not math.isinf(p[0]) and not math.isnan(
                    p[0]):  # got reflection for ray

                # convert to discrete coordinates
                cell_x = int(p[0] / self.resolution[0])
                # move own vehicle to the middle of the grid
                cell_y = int(p[1] / self.resolution[1] + len(self.grid[0]) / 2)

                if cell_x < len(self.grid) and cell_x >= 0 and \
                   cell_y < len(self.grid[0]) and cell_y >= 0:  # is cell in data structure
                    for free_cell in self.bresenham(
                        [0, int(len(self.grid[0]) / 2)],
                        [cell_x, cell_y])[:-1]:
                        self.grid[free_cell[0], free_cell[1]] = self.bayes(
                            self.grid[free_cell[0], free_cell[1]],
                            0.4)  # update all up to the reflection

                    self.grid[cell_x,
                              cell_y] = self.bayes(self.grid[cell_x, cell_y],
                                                   0.8)  # update reflection
                    # self.get_logger().info("update cell %d, %d to %f" %
                    #                       (cell_x, cell_y, self.grid[cell_x, cell_y]))

    def show(self):
        # show grid
        grayscale = np.array(self.grid * (255.0), dtype=np.uint8)
        bgra = cv2.cvtColor(grayscale, cv2.COLOR_GRAY2BGRA)
        bridge = CvBridge()
        return bridge.cv2_to_imgmsg(bgra, encoding="passthrough")

    def bayes(self, value, update):  # binary bayes formular
        return (value * update) / (value * update + (1.0 - value) *
                                   (1.0 - update))

    # based on https://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm
    def bresenham(self, point_from, point_to):
        ret = []

        dx = abs(point_to[0] - point_from[0])
        sx = 1 if point_from[0] < point_to[0] else -1
        dy = -abs(point_to[1] - point_from[1])
        sy = 1 if point_from[1] < point_to[1] else -1
        error = dx + dy

        while True:
            ret.append([point_from[0], point_from[1]])
            if point_from[0] == point_to[0] and point_from[1] == point_to[1]:
                return ret
            e2 = 2 * error
            if e2 >= dy:
                if point_from[0] == point_to[0]:
                    return ret
                error = error + dy
                point_from[0] = point_from[0] + sx
            if e2 <= dx:
                if point_from[1] == point_to[1]:
                    return ret
                error = error + dx
                point_from[1] = point_from[1] + sy

    def degrade(self):
        denominatorValue = 1.0 + self.degradeValue
        for x in range(len(self.grid)):
            for y in range(len(self.grid[x])):
                if self.grid[x, y] != 0.5:  # if not undecided
                    if self.grid[x, y] < 0.5:  # if below .5 add some value
                        self.grid[x, y] = 0.5 - \
                            ((0.5-self.grid[x, y])/denominatorValue)
                    else:  # if greater than .5 substract a bit
                        self.grid[x, y] = 0.5 + \
                            ((self.grid[x, y]-0.5)/denominatorValue)


def main(args=None):
    rclpy.init(args=args)
    agent = LIDARGrid()
    rclpy.spin(agent)
    rclpy.shutdown()


if __name__ == '__main__':
    main()
